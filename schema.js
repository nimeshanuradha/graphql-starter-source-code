const axios = require('axios');
const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLInt,
    GraphQLList,
    GraphQLSchema,
    GraphQLNonNull
} = require('graphql');


// Person Type
const PersonType = new GraphQLObjectType({
    name: 'person',
    fields: () => ({
        id: { type: GraphQLString },
        name: { type: GraphQLString },
        full_name: { type: GraphQLString },
        city: { type: GraphQLString },
        age: { type: GraphQLString },

        posts: {
            type: GraphQLList(PostType),
            resolve(person) {
                return axios.get('http://localhost:3000/posts/?authorID=' + person.id)
                    .then(res => res.data)
            }

        },
    })
})

// Post Type
const PostType = new GraphQLObjectType({
    name: 'post',
    fields: () => ({
        id: { type: GraphQLString },
        title: { type: GraphQLString },
        domain: { type: GraphQLString },
        rating: { type: GraphQLInt },
        content: { type: GraphQLString },

        author: {
            type: PersonType,
            resolve(post) {
                return axios.get('http://localhost:3000/persons/' + post.authorID)
                    .then(res => res.data)
            }
        },
    })
})



// Root Query
const Query = new GraphQLObjectType({
    name: 'Query',
    fields: {

        // Get all Persons 
        persons: {
            type: new GraphQLList(PersonType),
            resolve() {
                return axios.get('http://localhost:3000/persons')
                    .then(res => res.data)
            }
        },

        // Get a single Person
        person: {
            type: PersonType,
            args: {
                id: { type: GraphQLString }
            },
            resolve(parentValue, args) {
                return axios.get('http://localhost:3000/persons/' + args.id)
                    .then(res => res.data)
            }
        },

        // Get all Posts
        posts: {
            type: new GraphQLList(PostType),
            resolve() {
                return axios.get('http://localhost:3000/posts')
                    .then(res => res.data)
            }
        },

        // Get a single Post
        post: {
            type: PostType,
            args: {
                id: { type: GraphQLString }
            },
            resolve(parentValue, args) {
                return axios.get('http://localhost:3000/posts/' + args.id)
                    .then(res => res.data)
            }
        },


    }

})

// Root Mutation
const Mutations = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        addPost: {
            type: PostType,
            args: {
                authorID: { type: GraphQLInt },
                title: { type: GraphQLString },
                content: { type: GraphQLString },
                domain: { type: GraphQLString }
            },
            resolve(parentValue, args) {
                return axios.post('http://localhost:3000/posts', {
                    authorID: args.authorID,
                    title: args.title,
                    content: args.content,
                    domain: args.domain
                })
                    .then(res => res.data)
            }
        }
    }
})


module.exports = new GraphQLSchema({
    query: Query,
    mutation: Mutations
})